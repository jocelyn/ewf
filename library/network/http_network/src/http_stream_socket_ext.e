note
	description: "[
			Extension to HTTPD_STREAM_SOCKET to support backward compatibility.
			
			TO BE REMOVED IN THE FUTURE, When there is no need to support older compilers.
		]"

deferred class
	HTTP_STREAM_SOCKET_EXT

note
	copyright: "2011-2017, Jocelyn Fiat, Javier Velilla, Olivier Ligot, Colin Adams, Eiffel Software and others"
	license: "Eiffel Forum License v2 (see http://www.eiffel.com/licensing/forum.txt)"
	source: "[
			Eiffel Software
			5949 Hollister Ave., Goleta, CA 93117 USA
			Telephone 805-685-1006, Fax 805-685-6869
			Website http://www.eiffel.com
			Customer support http://support.eiffel.com
		]"
end
